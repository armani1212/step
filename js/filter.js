$(document).ready(function () {

    $(".item").hide();
    $(".item").slice(0, 12).show();
    $(".btn-show-more").click(function () {
        const imageType = $(".btn.active").data("filter");
        // console.log($(".btn.active").data('filter'));
        $(`.item${imageType}:hidden`).slice(0, 12).show();
    });

    $('.btn').click(function () {
        $(this).addClass('active').siblings().removeClass("active");
        const imageType = $(this).data("filter");
        //console.log(imageType);
        $(".item").hide();

        $(`.item${imageType}`).slice(0, 12).show();

    });
});