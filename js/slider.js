// slider
$('.slider-p').hide().first().show();
$('.slider-name').hide().first().show();
$('.position-employee').hide().first().show();
$('.focus-img').hide().first().show();

let autoSlideID = slideAuto(3000);

$('.slider-img-all').on('click', function () {
    $(this)
        .addClass('active')
        .siblings()
        .removeClass('active');

    changeInfo();
    clearInterval(autoSlideID);
    autoSlideID = slideAuto(10000);
});

$('.slider-arrow').on('click', function () {
    if ($(this).hasClass('left')) {
        const activeIndex = $('.slider-img-all.active').index();
        const newIndex = (activeIndex === 0) ? 3 : activeIndex - 1;

        $(`.slider-img-all:eq(${newIndex})`)
            .addClass('active')
            .siblings()
            .removeClass('active');

        changeInfo();
        clearInterval(autoSlideID);
        autoSlideID = slideAuto(5000);
    }
    if ($(this).hasClass('right')) {
        const activeIndex = $('.slider-img-all.active').index();
        const newIndex = (activeIndex === 3) ? 0 : activeIndex + 1;

        $(`.slider-img-all:eq(${newIndex})`)
            .addClass('active')
            .siblings()
            .removeClass('active');

        changeInfo();
        clearInterval(autoSlideID);
        autoSlideID = slideAuto(5000);
    }
});

function slideAuto(interval) {
    return setInterval(function () {
        const activeIndex = $('.slider-img.active').index();
        const newIndex = (activeIndex === 3) ? 0 : activeIndex + 1;

        $(`.slider-img-all:eq(${newIndex})`)
            .addClass('active')
            .siblings()
            .removeClass('active');

        changeInfo();
    }, interval);
}

function changeInfo() {
    const activeIndex = $('.slider-img-all.active').index();

    $(`.slider-p:eq(${activeIndex})`)
        .fadeIn(1600)
        .siblings('.slider-p')
        .hide();
    $(`.slider-name:eq(${activeIndex})`)
        .fadeIn(1200)
        .siblings('.slider-name')
        .hide();
    $(`.position-employee:eq(${activeIndex})`)
        .fadeIn(800)
        .siblings('.position-employee')
        .hide();
    $(`.focus-img:eq(${activeIndex})`)
        .fadeIn(400)
        .siblings('.focus-img')
        .hide();
}